const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send({
    data: {
      proyecto: "SCI",
      materia: "Proyecto Final"
    }
  });
});

app.listen(5000);
